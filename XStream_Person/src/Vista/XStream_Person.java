/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Person;
import Modelo.PhoneNumber;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 *
 * @author jorge
 */
public class XStream_Person {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        XStream xstream = new XStream(new StaxDriver());
        
        xstream.alias("person", Person.class);
        xstream.alias("phonenumber",PhoneNumber.class);
        
        Person joe = new Person("Joe","Walnes");
        joe.setPhone(new PhoneNumber(123,"1234-456"));
        joe.setFax( new PhoneNumber(123,"9999-999"));
        
        String xml = xstream.toXML(joe);
        System.out.println(xml);
    }
    
}
