/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jorge
 */
public class Alumno implements Serializable{
    private int id;
    private String nombre;
    private float sueldo;
    private boolean registrado;
    private Date fechanacimiento;
    private Date horatutoria;
    private Date fecharegistro;
    private String observaciones;

    public Alumno() {
    }

    
    public Alumno(int id, String nombre, float sueldo, boolean registrado, Date fechanacimiento, Date horatutoria,Date fecharegistro, String oservaciones) {
        this.id = id;
        this.nombre = nombre;
        this.sueldo = sueldo;
        this.registrado = registrado;
        this.fechanacimiento = fechanacimiento;
        this.horatutoria = horatutoria;
        this.fecharegistro = fecharegistro;
        this.observaciones = oservaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public boolean isRegistrado() {
        return registrado;
    }

    public void setRegistrado(boolean registrado) {
        this.registrado = registrado;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public Date getHoratutoria() {
        return horatutoria;
    }

    public void setHoratutoria(Date horatutoria) {
        this.horatutoria = horatutoria;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String oservaciones) {
        this.observaciones = oservaciones;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }
    
    
}
