/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import model.Components.Coberturas;
import model.Components.Enfermedades;
import model.Components.Nif;
import model.Enums.Sexo;

/**
 *
 * @author jorge
 */
public class Seguro implements Serializable {

 private Integer idSeguro;
    private Nif nif;
    private String nombre;
    private String ape1;
    private String ape2;
    private Integer edad;
    private Sexo sexo;
    private Boolean casado;
    private Integer numHijos;
    private Boolean embarazada;
    private Coberturas cober;
    private Enfermedades enfer;
    private String nombreAlergia;
    private Date fechaCreacion;
    private Set asistenciamedicas = new HashSet(0);

    public Seguro() {
    }

    public Seguro(Integer idSeguro, String nombre, String ape1, String ape2, Integer edad, Boolean casado, Integer numHijos, Boolean embarazada, Date fechaCreacion) {
        this.idSeguro = idSeguro;
        this.nombre = nombre;
        this.ape1 = ape1;
        this.ape2 = ape2;
        this.edad = edad;
        this.casado = casado;
        this.numHijos = numHijos;
        this.embarazada = embarazada;
        this.fechaCreacion = fechaCreacion;

    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Integer getIdSeguro() {
        return this.idSeguro;
    }

    public Coberturas getCober() {
        return cober;
    }

    public void setCober(Coberturas cober) {
        this.cober = cober;
    }

    public void setIdSeguro(Integer idSeguro) {
        this.idSeguro = idSeguro;
    }

    public Nif getNif() {
        return this.nif;
    }

    public void setNif(Nif nif) {
        this.nif = nif;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApe1() {
        return this.ape1;
    }

    public void setApe1(String ape1) {
        this.ape1 = ape1;
    }

    public String getApe2() {
        return this.ape2;
    }

    public void setApe2(String ape2) {
        this.ape2 = ape2;
    }

    public Integer getEdad() {
        return this.edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Boolean getCasado() {
        return this.casado;
    }

    public void setCasado(Boolean casado) {
        this.casado = casado;
    }

    public Integer getNumHijos() {
        return this.numHijos;
    }

    public void setNumHijos(Integer numHijos) {
        this.numHijos = numHijos;
    }

    public Boolean getEmbarazada() {
        return this.embarazada;
    }

    public void setEmbarazada(Boolean embarazada) {
        this.embarazada = embarazada;
    }

    public Enfermedades getEnfer() {
        return enfer;
    }

    public void setEnfer(Enfermedades enfer) {
        this.enfer = enfer;
    }

    public String getNombreAlergia() {
        return this.nombreAlergia;
    }

    public void setNombreAlergia(String nombreAlergia) {
        this.nombreAlergia = nombreAlergia;
    }

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Set getAsistenciamedicas() {
        return this.asistenciamedicas;
    }

    public void setAsistenciamedicas(Set asistenciamedicas) {
        this.asistenciamedicas = asistenciamedicas;
    }

}
