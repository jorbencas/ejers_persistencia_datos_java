/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import model.AsistenciaMedica;
import model.Components.Coberturas;
import model.Components.Enfermedades;
import model.Components.Nif;
import model.Enums.Sexo;
import model.Enums.TipoAsistencia;
import model.Seguro;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //CREAMOS CONEXION
        //SessionFactory sessionFactory;
        //Configuration configuration = new Configuration();
        //configuration.configure();
        //sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //Profesor profesor=new Profesor(7,"Pepe","Garci1a","Perez");
        Nif  nif= new Nif("45688737P");
        Coberturas cober = new Coberturas(false, false, false);
        Enfermedades enfer = new Enfermedades(false, false, false, false, "Ninguna alergia");

        Seguro seg = new Seguro(437, "Jorge", "Beneyto", "Castelló", 20, false, 0, false, Calendar.getInstance().getTime());
        seg.setNif(nif);
        seg.setCober(cober);
        seg.setEnfer(enfer);
        seg.setSexo(Sexo.Hombre);

        
        Set<AsistenciaMedica> asistencias = new HashSet<>();
        AsistenciaMedica asis = new AsistenciaMedica(seg, "Estoy resfriado", "Mi casa", "Me resfrie", Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), new BigDecimal(1000), 2);
        asis.setAsis(TipoAsistencia.Domiciliaria);
        asistencias.add(asis);

        seg.setAsistenciamedicas(asistencias);
        //CREAR UNA SESION
        Session session = factory.openSession();
        session.beginTransaction();

        //GUARDAR OBJETO
        session.saveOrUpdate(asis);
        session.saveOrUpdate(seg);

        //CERRAR CONEXION
        session.getTransaction().commit();
        session.close();
        factory.close();

    }

}
