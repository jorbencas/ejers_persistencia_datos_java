package Vista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import Controlador.Book_Ctrl;
import Controlador.BookStore_Ctrl;
import Controlador.DOM_Ctrl;
import Modelo.Book;
import Modelo.BookStore;

public class Main {

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
	     File f1 = new File("bookstore.xml");
	     File f2 = new File("bookstore_jorge_beneyto_castello.xml");
	     BookStore_Ctrl bookread = new BookStore_Ctrl();
	     BookStore_Ctrl bookwrite = new BookStore_Ctrl();
	     Book_Ctrl bookElem = new Book_Ctrl();
	     BookStore bookStore = new BookStore();
	     BookStore booksStore2 = new BookStore();
	     Document doc = (Document) bookread.recuperar(f1);
	     bookStore = bookread.llegir(doc);
	     System.out.println(bookStore);
	     

	     DOM_Ctrl xtdom = new DOM_Ctrl();
	     Document doc2 = (Document) xtdom.InstanciaDocumento();
	     bookElem.librosdeEjemplo(booksStore2);
	     bookwrite.escriure(doc2, booksStore2);
	     bookwrite.enmagatzemar(doc2, f2);
	     muestraFichero(f2);
	}
	
	  static void mostrarMenu() {
	        System.out.println("1.-Introducir ruta xml y instanciar documento");
	        System.out.println("2.-Leer el documento y crear objeto Bookstore");
	        System.out.println("3.-Imprimir el bookstore y todos sus datos");
	        System.out.println("****************************************************************************");
	        System.out.println("4.-Dar de alta la tienda de libros y un par de libros de ejemplo");
	        System.out.println("5.-Escribir el objeto bookstore en un nuevo documento vacio");
	        System.out.println("6.-Seleccionar nombre fichero destino");
	        System.out.println("7.-Guardar el documento");
	    }
	  
	  static void muestraFichero(File archivo) {

	        FileReader fr = null;
	        BufferedReader br = null;

	        try {

	            fr = new FileReader(archivo);
	            br = new BufferedReader(fr);

	            // Lectura del fichero
	            String linea;
	            while ((linea = br.readLine()) != null) {
	                System.out.println(linea);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {

	            try {
	                if (null != fr) {
	                    fr.close();
	                }
	            } catch (Exception e2) {
	                e2.printStackTrace();
	            }
	        }
	    }
}
