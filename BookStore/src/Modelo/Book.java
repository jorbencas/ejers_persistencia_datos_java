package Modelo;

import java.util.ArrayList;
import java.util.function.Consumer;

public class Book {
	private String title = "";
	private String lang = "";
	private ArrayList<Autor> author = new ArrayList<Autor>();
	private String fecha_publicacion = "";
	private String category = "";
	private String cover = "";
	private Double precio = 0.00;
	
	public Book (){}

	public Book(String title, String lang, String fecha_publicacion, String category,
			String cover, Double precio) {
		super();
		this.title = title;
		this.lang = lang;
		this.fecha_publicacion = fecha_publicacion;
		this.category = category;
		this.cover = cover;
		this.precio = precio;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	
	public ArrayList<Autor> getAuthores() {
		return author;
	}

	public String listarAuthor() {
		String autor = "";
		
		for (int i = 0; i < this.getAuthores().size(); i++){
			 autor += this.getAuthores().get(i).getAuthor().toString();
			 autor = this.getAuthores().size() == 1 ? autor : autor + ", ";
		} 
		
		return  autor;
	}
	public void setAuthor(Autor author) {
		this.author.add(author);
	}
	
	public String getFecha_publicacion() {
		return fecha_publicacion;
	}
	public void setFecha_publicacion(String fecha_publicacion) {
		this.fecha_publicacion = fecha_publicacion;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCover() {
		return cover;
	}
	public void setCover(String cover) {
		this.cover = cover;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	@Override
	public String toString() {
		String result = "";
		result = "Book [" + " title=" + title + ", lang=" + lang + ", Autor: " + this.listarAuthor()
		+ ", fecha_publicacion=" + fecha_publicacion + ", category=" + category + ", cover=" + cover
		+ ", precio=" + precio + "]"; 

		return result;
	}
	
	 
	
	
}
