package Modelo;

import java.util.ArrayList;

public class BookStore extends ArrayList<Book> {

	@Override
    public String toString() {
        String result = "";
        for (int i = 0; i < this.size(); i++) {

            result += "\n" + this.get(i).toString();
        }
        
        return result;

    }
}
