package Modelo;


public class Autor {

	private String nombre = "";
	
	
	public Autor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Autor(String nombre) {
		this.nombre = nombre;
	}

	public String getAuthor() {
		return this.nombre;
	}
	
	public void setAuthor(String nombre) {
		this.nombre = nombre;
	}
	
}
