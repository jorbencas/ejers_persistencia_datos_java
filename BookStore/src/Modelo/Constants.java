package Modelo;

public class Constants {
	public static final String ET_BOOKSTORE = "bookstore";
	public static final String ET_BOOK = "book";
	public static final String ET_AUTHORES = "authores";
	public static final String ET_AUTHOR = "author";
	public static final String ET_PRICE = "price";
	public static final String ET_CATEGORY = "category";
	public static final String ET_COVER = "cover";
	public static final String ET_TITULO = "title";
	public static final String ET_YEAR = "year";
	public static final String ET_LANG = "lang";
}
