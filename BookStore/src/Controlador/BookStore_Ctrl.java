package Controlador;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Modelo.BookStore;
import Modelo.Constants;
import Modelo.Book;
import Controlador.DOM_Ctrl;

public class BookStore_Ctrl extends DOM_Ctrl {
	File file = null;
	private BookStore Books = null;

	public BookStore_Ctrl() {
		Books = new BookStore();
	}

	public BookStore_Ctrl(BookStore Books) {
		this.Books = Books;
	}

	public BookStore_Ctrl(BookStore Books, File file) {
		this.file = file;
		this.Books = Books;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}


	public Document recuperar(File xmlFile) throws SAXException, IOException, ParserConfigurationException{
		Document doc = this.InstanciaDocumento(xmlFile);
		return doc;
	}

	public BookStore llegir (Document doc){
		Element elBooks = doc.getDocumentElement();
		NodeList listaBooks = elBooks.getChildNodes();

		for (int i = 0; i < listaBooks.getLength(); i++) {
			if(listaBooks.item(i).getNodeType() == Node.ELEMENT_NODE){
				Books.add(Book_Ctrl.llegir((Element) listaBooks.item(i)));
			}
		}
		return Books;
	}

	public void escriure(Document doc, BookStore bookstore) {
		Element arrel = doc.createElement(Constants.ET_BOOKSTORE);
		doc.appendChild(arrel);
		for (Book f : bookstore) {
			Book_Ctrl.escriure(f, arrel, doc);
		}
	}

	public void enmagatzemar(Document doc, File file) throws TransformerException{
		DOM_Ctrl.guardarXML(doc, file);
	}
 
}
