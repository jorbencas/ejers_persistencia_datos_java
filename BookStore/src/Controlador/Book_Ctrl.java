package Controlador;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Modelo.Autor;
import Modelo.Book;
import Modelo.BookStore;
import Modelo.Constants;

public class Book_Ctrl extends DOM_Ctrl {

	public static Book llegir(Element Book){
		Book book = new Book();
		Element titulo = getElementEtiqueta(Constants.ET_TITULO, Book);

		book.setCategory(getAtributoEtiqueta(Constants.ET_CATEGORY,Book));
		book.setCover(getAtributoEtiqueta(Constants.ET_COVER,Book));
		book.setTitle(getValorEtiqueta(Constants.ET_TITULO,Book));
		book.setLang(getAtributoEtiqueta(Constants.ET_LANG,titulo));
		llegirAutors(getElementEtiqueta(Constants.ET_AUTHORES, Book), book);
		book.setPrecio(Double.parseDouble(getValorEtiqueta(Constants.ET_PRICE,Book)));
		book.setFecha_publicacion(getValorEtiqueta(Constants.ET_YEAR,Book));

		return book;
	}

	public static void llegirAutors(Element autores, Book book){//element etiqueta autores
		NodeList author = autores.getChildNodes();

		for (int i = 0; i < author.getLength(); i++) {
			if(author.item(i).getNodeType() == Node.ELEMENT_NODE){
				book.setAuthor(new Autor(autores.getChildNodes().item(i).getTextContent()));
			}
		}
		
	}


	public static void escriure(Book Book, Element arrel, Document doc){
		Element elemBook = doc.createElement(Constants.ET_BOOK);
		escriureBook( Book, elemBook, doc);
		arrel.appendChild(elemBook);
	}

	protected static void escriureBook(Book book, Element elementobook, Document doc){
		Attr attrCategory = doc.createAttribute(Constants.ET_CATEGORY);
		Attr attrCover = doc.createAttribute(Constants.ET_COVER);
		elementobook.setAttributeNode(attrCategory);
		attrCategory.setValue(book.getCategory());

		if(book.getCover() != "" ){
			elementobook.setAttributeNode(attrCover);
			attrCover.setValue(book.getCover());
		}

		Element elmTitle = doc.createElement(Constants.ET_TITULO);
		Attr attrLang =  doc.createAttribute(Constants.ET_LANG);
		elementobook.appendChild(elmTitle);
		elmTitle.appendChild(doc.createTextNode(book.getTitle()));
		elmTitle.setAttributeNode(attrLang);
		attrLang.setValue(book.getLang());

		Element elmAuthores = doc.createElement(Constants.ET_AUTHORES);
		elementobook.appendChild(elmAuthores);
		for (int i = 0; i < book.getAuthores().size(); i++){
			Element elmAuthor = doc.createElement(Constants.ET_AUTHOR);
			elmAuthores.appendChild(elmAuthor);
			elmAuthor.appendChild(doc.createTextNode(book.getAuthores().get(i).getAuthor().toString()));
		} 

		Element elmPrice = doc.createElement(Constants.ET_PRICE);
		elementobook.appendChild(elmPrice);
		elmPrice.appendChild(doc.createTextNode(String.valueOf(book.getPrecio())));

		Element elmYear = doc.createElement(Constants.ET_YEAR);
		elementobook.appendChild(elmYear);
		elmYear.appendChild(doc.createTextNode(book.getFecha_publicacion()));
	}

	public BookStore librosdeEjemplo(BookStore bookStore) {

		Book libroEjemplar1 = new Book( "EL SECRETO DE LAS HERMANAS BLACKWOOD", "es", "2008",  "thriller", "pdf", 20.00);
		libroEjemplar1.getAuthores().add(new Autor("Ell�n Marte Wisman"));

		Book libroEjemplar2 = new Book( "ALG� QUE NO HI HAVIA DE SER", "es", "2018", "mistery", "", 18.09);
		libroEjemplar2.getAuthores().add(new Autor("Sandra Cabanes"));

		Book libroEjemplar3 = new Book( "Falso Nueve",  "es", "2008", "Trchnologi", "paperback", 28.99);
		libroEjemplar3.getAuthores().add(new Autor("Pepe Caballero"));
		libroEjemplar3.getAuthores().add(new Autor("Isabel Sanz"));

		bookStore.add(libroEjemplar2);
		bookStore.add(libroEjemplar3);
		return bookStore;
	}

}
