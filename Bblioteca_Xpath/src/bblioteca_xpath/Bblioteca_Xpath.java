/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bblioteca_xpath;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class Bblioteca_Xpath {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
        // TODO code application logic here
        String nombreArchivo = "Biblioteca.xml";
        File Fxmlfile1 = new File(nombreArchivo);
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(Fxmlfile1);
        
        doc.getDocumentElement().normalize();
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        NodeList ListArticulo = null;
        
        try {
             //ListArticulo = (NodeList) xpath.evaluate("/biblioteca/libro/titulo", doc,XPathConstants.NODESET);
              ListArticulo = (NodeList) xpath.evaluate("/biblioteca/libro/titulo", doc,XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < ListArticulo.getLength(); i++) {
            if(ListArticulo.item(i) != null){
                System.out.println(ListArticulo.item(i).getTextContent());
            }
        }
    }
    
}
