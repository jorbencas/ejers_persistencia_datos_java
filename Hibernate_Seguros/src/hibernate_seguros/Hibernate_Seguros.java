/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernate_seguros;

import Modelo.Seguro;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 *
 * @author jorge
 */
public class Hibernate_Seguros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion = 999;
        Scanner teclado = new Scanner(System.in);
        String nif, nombre, ape1, ape2, respuesta;
        int edad, hijos, id;
        Seguro seguro = null;

        SessionFactory sessionFactory;

        Configuration configuration = new Configuration();
        configuration.addClass(Seguro.class);

        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            while (opcion != 0) {
                mostrarMenu();
                opcion = teclado.nextInt();
                teclado.nextLine();
                switch (opcion) {
                    case 1:
                        System.out.println("Introduzca NIF");
                        nif = teclado.nextLine();
                        System.out.println("Introduzca Nombre");
                        nombre = teclado.nextLine();
                        System.out.println("Introduzca apellidos primeros");
                        ape1 = teclado.nextLine();
                        System.out.println("Introduzca apellidos segundos");
                        ape2 = teclado.nextLine();
                        System.out.println("Introduzca edad");
                        edad = teclado.nextInt();
                        teclado.nextLine();
                        System.out.println("Introduzca numero de hijos ");
                        hijos = teclado.nextInt();
                        seguro = new Seguro(nif, nombre, ape1, ape2, edad, hijos, Calendar.getInstance().getTime());
                        session.beginTransaction();
                        session.save(seguro);
                        session.getTransaction().commit();
                        break;
                        
                    case 2:
                        System.out.println("Introduzca ID");
                        id = teclado.nextInt();
                        teclado.nextLine();
                        session.beginTransaction();
                        Query query = session.createQuery("from Seguro where idSeguro = :id ");
                        query.setParameter("id", id);
                        ArrayList<?> list = (ArrayList<?>) query.list();
                        seguro = (Seguro) list.get(0);
                        //    System.out.println(seguro);
                        session.getTransaction().commit();
                        if (seguro != null) {
                            System.out.println("Recuperado " + seguro);
                        } else {
                            System.out.println("No existe el objeto");
                        }
                        break;
                        
                    case 3:
                        if (seguro != null) {
                            System.out.println("Deseas usar el objeto que seleccionaste anteriormente? S/N \n " + seguro.toString());
                            respuesta = teclado.nextLine();
                            if (respuesta.equalsIgnoreCase("N")) {
                                System.out.println("Introduzca ID del objeto a actualizar");
                                id = teclado.nextInt();
                                teclado.nextLine();
                                session.beginTransaction();
                                query = session.createQuery("from Seguro where idSeguro = :id ");
                                query.setParameter("id", id);
                                list = (ArrayList<?>) query.list();
                                seguro = (Seguro) list.get(0);
                                //    System.out.println(seguro);
                                session.getTransaction().commit();
                                
                                System.out.println("Recuperado " + seguro);
                            }
                        } else {
                            System.out.println("Introduzca ID del objeto a actualizar");
                            id = teclado.nextInt();
                            session.beginTransaction();
                            query = session.createQuery("from Seguro where idSeguro = :id ");
                            query.setParameter("id", id);
                            list = (ArrayList<?>) query.list();
                            seguro = (Seguro) list.get(0);
                            //    System.out.println(seguro);
                            session.getTransaction().commit();
                            System.out.println("Recuperado " + seguro);
                        }
                        System.out.println("Introduzca NIF");
                        seguro.setNif(teclado.nextLine());
                        System.out.println("Introduzca Nombre");
                        seguro.setNombre(teclado.nextLine());
                        System.out.println("Introduzca apellidos primeros");
                        seguro.setApe1(teclado.nextLine());
                        System.out.println("Introduzca apellidos segundos");
                        seguro.setApe2(teclado.nextLine());
                        System.out.println("Introduzca edad");
                        seguro.setEdad(teclado.nextInt());
                        teclado.nextLine();
                        System.out.println("Introduzca numero de hijos ");
                        seguro.setNumHijos(teclado.nextInt());
                        teclado.nextLine();
                        session.beginTransaction();
                        session.update(seguro);
                        session.getTransaction().commit();
                        System.out.println("Actualizado con exito!");
                        break;
                        
                    case 4:
                        System.out.println("Introduzca ID del objeto a eliminar");
                        id = teclado.nextInt();
                        teclado.nextLine();
                        session.beginTransaction();
                        query = session.createQuery("from Seguro where idSeguro = :id ");
                        query.setParameter("id", id);
                        list = (ArrayList<?>) query.list();
                        seguro = (Seguro) list.get(0);
                        session.getTransaction().commit();
                        if (seguro != null) {
                            System.out.println("Eliminado con exito " + seguro.toString());
                            session.beginTransaction();
                            session.delete(seguro);
                            session.getTransaction().commit();
                        } else {
                            System.out.println("No existe el objeto");
                        }
                        break;
                    case 5:
                        session.close();
                        System.exit(0);
                        break;
                        default:
                            System.out.println("Opción incorrecta, intentalo de nuevo.");
                            break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Error en la conxion de a la base de datos: " + ex.getMessage());
        }

    }

    public static void mostrarMenu() {
        System.out.println("1.- Insertar un objeto Seguro en la BBDD");
        System.out.println("2.- Leer un objeto Seguro de la BBDD");
        System.out.println("3.- Actualizar un objeto Seguro de la BBDD");
        System.out.println("4.- Borrar un objeto Seguro de la BBDD");
        System.out.println("5.~ Salir");
    }

}
