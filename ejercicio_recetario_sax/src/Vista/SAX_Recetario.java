/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ManejadorDeRecetario;
import Modelo.Recetario;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author jorge
 */
public class SAX_Recetario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Recetario recetario = new Recetario();
        int opcion = 777;
        Scanner teclado = new Scanner(System.in);
        String ruta = "recetario.xml";
        String entrada = "";

        while (opcion != 0) {
            menu();
            opcion = teclado.nextInt();
            switch (opcion) {

                case 1:
                    teclado.nextLine();
                    System.out.println("Introduzca ruta o dejalo en blanco para usar la ruta por defecto");
                    entrada = teclado.nextLine();
                    if (entrada.length() != 0) {
                        ruta = entrada;
                    }
                    break;

                case 2:
                    try {
                        XMLReader reader = XMLReaderFactory.createXMLReader();
                        reader.setContentHandler(new ManejadorDeRecetario(recetario));
                        reader.parse(new InputSource(new FileInputStream(ruta)));
                    } catch (FileNotFoundException ex) {
                       ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    break;

                case 3:
                    recetario.imprimirRecetas();
                    break;
            }
        }
    }
    
     public static void menu() {
        System.out.println("1.- Elegir fichero xml");
        System.out.println("2.- Leer el fichero y crear recetario");
        System.out.println("3.- Mostrar el recetario");
    }
}
