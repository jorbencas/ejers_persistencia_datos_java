/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author jorge
 */
public class Informacion_general {
    private int comensales = 0;
    private String unidad = "";
    private int tiempo = 0;
    private String dificultat = "";

    public Informacion_general() {
    }

    public int getComensales() {
        return comensales;
    }

    public void setComensales(int comensales) {
        this.comensales = comensales;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public String getDificultat() {
        return dificultat;
    }

    public void setDificultat(String dificultat) {
        this.dificultat = dificultat;
    }

    @Override
    public String toString() {
        return "Informacio_general{" + "comensales=" + comensales + ", unidad=" + unidad + ", tiempo=" + tiempo + ", dificultat=" + dificultat + '}';
    }
    
}
