/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class Recetario extends ArrayList<Receta> {
    
    public void imprimirRecetas() {
        for (int i = 0; i < this.size(); i++) {
            this.get(i).imprimirFormateado(i);
        }
    }
}
