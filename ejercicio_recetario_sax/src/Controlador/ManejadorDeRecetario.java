/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Informacion_general;
import Modelo.Ingrediente;
import Modelo.Paso;
import Modelo.Receta;
import Modelo.Recetario;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author jorge
 */
public class ManejadorDeRecetario extends DefaultHandler {
    
    private String valor;
    private Recetario recetario;
    private Receta recetita;
    private Informacion_general ingrediente;
    private Ingrediente ingr;
    private Paso paso; 

    public ManejadorDeRecetario(Recetario recetario) {
        this.recetario = recetario;
    }

    
    @Override
    public void startDocument() throws SAXException {
        //super.startDocument(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //super.startElement(uri, localName, qName, attributes); //To change body of generated methods, choose Tools | Templates.
        //System.out.println(localName);
        
        if (localName.equals("receta")) {
            recetita = new Receta();
            this.recetario.add(recetita);
        }
        if (localName.equals("informacion_general")) {
            ingrediente = new Informacion_general();
            this.recetita.setInformacion_general(ingrediente);
        }
        if (localName.equals("ingrediente")) {
            ingr = new Ingrediente();
            ingr.setCantidad(attributes.getValue("cantidad"));
            ingr.setUnidad(attributes.getValue("unidad"));
            this.recetita.getListaIngredientes().add(ingr);
        }
        if (localName.equals("paso")) {
            paso = new Paso();
            paso.setNumero(attributes.getValue("numero"));
            this.recetita.getListaPasos().add(paso);
        }
        if (localName.equalsIgnoreCase("tiempo")) {
            ingrediente.setUnidad(attributes.getValue("unidad"));
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        //super.characters(ch, start, length); //To change body of generated methods, choose Tools | Templates.
        this.valor = new String(ch, start, length);
        //System.out.println("Hola " + this.valor);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        //super.endElement(uri, localName, qName); //To change body of generated methods, choose Tools | Templates.
        if (localName.equalsIgnoreCase("nombre")) {
            this.recetita.setNombre(this.valor);
        }
        if (localName.equalsIgnoreCase("categoria")) {
            this.recetita.setCategoria(this.valor);
        }
        if (localName.equalsIgnoreCase("comensales")) {
            this.ingrediente.setComensales(Integer.parseInt(this.valor));
        }
        if (localName.equalsIgnoreCase("tiempo")) {
            this.ingrediente.setTiempo(Integer.parseInt(this.valor));
        }
        if (localName.equalsIgnoreCase("dificultad")) {
            this.ingrediente.setDificultat(this.valor);
        }
        if (localName.equalsIgnoreCase("ingrediente")) {
            this.ingr.setNombre(this.valor);
        }
        if (localName.equalsIgnoreCase("paso")) {
            this.paso.setDescripcion(this.valor);
        }
        //System.out.println(" FIN" + this.recetita );
    }

    @Override
    public void endDocument() throws SAXException {
        //super.endDocument(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
