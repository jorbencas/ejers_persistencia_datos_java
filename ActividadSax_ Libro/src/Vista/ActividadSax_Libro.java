/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ManejadorElementos;
import Modelo.Libro;
import java.io.FileInputStream;
import java.io.IOException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author jorge
 */
public class ActividadSax_Libro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException {
        // TODO code application logic here

        try {
            Libro libro = new Libro();
            XMLReader reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler((ContentHandler) new ManejadorElementos(libro));
            reader.parse(new InputSource(new FileInputStream("Libros.xml")));
        } catch (IOException e) {
            e.printStackTrace();
        } catch(SAXException e){
            e.printStackTrace();
        }
    }
    
}
