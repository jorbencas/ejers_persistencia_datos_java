/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Modelo.Dibuix;
import Modelo.Figura;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class ttlCtrlDibujo extends ttlCtrlDOM {
     static final String ET_DIBUIX = "dibuix";
    File file = null;
    private Dibuix dibuix = null;
    
    public ttlCtrlDibujo() {
        dibuix = new Dibuix();
    }

    public ttlCtrlDibujo(Dibuix dibuix) {
        this.dibuix = dibuix;
    }

    public ttlCtrlDibujo(Dibuix dibuix, File file) {
        this.file = file;
        this.dibuix = dibuix;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    /*public void recuperar() throws SAXException, IOException, ParserConfigurationException{
        Document doc = null;
        doc = this.XMLaDOM(file);
        this.llegir(doc);
    }*/
    
    public Document recuperar(File xmlFile) throws SAXException, IOException, ParserConfigurationException{
        Document doc = null;
        doc = this.XMLaDOM(xmlFile);
        return doc;
    }
    
    public Dibuix llegir (Document doc){
        Element eldibuix = doc.getDocumentElement();
        NodeList listafiguras = eldibuix.getChildNodes();
        
        for (int i = 0; i < listafiguras.getLength(); i++) {
            if(listafiguras.item(i).getNodeType() == Node.ELEMENT_NODE){
                 dibuix.add(CtrlFigura.llegir((Element) listafiguras.item(i)));
            }
        }
        return dibuix;
    }
    
     public void escriure(Document doc) {
        Element arrel = doc.createElement(ET_DIBUIX);
        doc.appendChild(arrel);
        for (Figura f : dibuix) {
            CtrlFigura.escriure(f, arrel, doc);
        }
    }
     /*
     public void enmagatzemar(){
         Document odc= null;
         doc = instamn
     }*/
     
     public void enmagatzemar(Document doc, File file) throws TransformerException{
         ttlCtrlDOM.DOMaXML(doc, file);
     }
     

}
