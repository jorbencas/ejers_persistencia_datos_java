/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Modelo.Rectangle;
import Modelo.Figura;
import Modelo.Cercle;
import java.awt.Color;
import Modelo.FiguraSimple;
import Modelo.TipusFigura;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author jorge
 */
public class CtrlFigura extends ttlCtrlDOM {
     //Constants amb els noms de les etiquetes
    static final String ET_POSICIO = "posicio";
    static final String ET_X = "x";
    static final String ET_Y = "y";
    static final String ET_ESCALA = "escala";
    static final String ET_ROTACIO = "rotacio";
    static final String ET_FIGURA = "figura";
    static final String ET_COLOR_LIN = "colorlinia";
    static final String ET_COLOR_FIG = "colorfigura";
    static final String ET_FIGURA_SIMPLE = "figurasimple";
    static final String ET_ALCADA = "alcada";
    static final String ET_AMPLADA = "amplada";
    static final String ET_RECTANGLE = "rectangle";
    static final String ET_GRUP = "grup";
    static final String ET_COLLECCIO = "colleccio";
    static final String ET_CERCLE = "cercle";
    static final String ET_RADI = "radi";
    
    public static Figura llegir(Element element){
       Figura res = null;
        if (element.getTagName().equalsIgnoreCase(ET_CERCLE)) {
            res = llegirCercle(element);
        } else if (element.getTagName().equalsIgnoreCase(ET_RECTANGLE)) {
            res = llegirRectangle(element);
        }
        return res;
    }
    
       public static Cercle llegirCercle(Element element) {
        Cercle c = new Cercle();
        Element elemFS = getElementEtiqueta(ET_FIGURA_SIMPLE, element);
        llegirFiguraSimple(elemFS, c);
        int valorI = Integer.parseInt(getValorEtiqueta(ET_RADI, element));
        c.setRadi(valorI);
        return c;
    }

    public static Rectangle llegirRectangle(Element element) {
        Rectangle r = new Rectangle();
        Element elemFS = getElementEtiqueta(ET_FIGURA_SIMPLE, element);
        llegirFiguraSimple(elemFS, r);
        int valorI = Integer.parseInt(getValorEtiqueta(ET_ALCADA, element));
        r.setAlcada(valorI);
        valorI = Integer.parseInt(getValorEtiqueta(ET_AMPLADA, element));
        r.setAmplada(valorI);
        return r;
    }
    
    public static void llegirFiguraSimple( Element elemFiguraSimple , FiguraSimple fs){
         Element elemFigura = getElementEtiqueta(ET_FIGURA, elemFiguraSimple);
        llegirFigura(elemFigura, fs);
        int valorI = Integer.parseInt(getValorEtiqueta(ET_COLOR_LIN, elemFiguraSimple));
        fs.setColorLinia(new Color(valorI));
        valorI = Integer.parseInt(getValorEtiqueta(ET_COLOR_FIG, elemFiguraSimple));
        fs.setColorFigura(new Color(valorI));
    }

    private static void llegirFigura(Element elemFigura, Figura fs) {
      Element posicio =  getElementEtiqueta(ET_POSICIO, elemFigura);
      Integer X = Integer.parseInt(getValorEtiqueta(ET_X,posicio));
      Integer Y = Integer.parseInt(getValorEtiqueta(ET_Y,posicio));
      fs.setPosicio(X,Y);
      fs.setEscala(Integer.parseInt(getValorEtiqueta(ET_ESCALA, elemFigura)));
      fs.setRotacio(Float.parseFloat(getValorEtiqueta(ET_ROTACIO, elemFigura)));
    }
    
    public static void escriure(Figura figura, Element arrel, Document doc){
       Element elemfigura;
       TipusFigura tipusfigura = figura.getTipusFigura();
       if( tipusfigura == TipusFigura.CERCLE){
            elemfigura = doc.createElement(ET_CERCLE);
            escriure((Cercle) figura, elemfigura, doc);
            arrel.appendChild(elemfigura);
        } else if (tipusfigura == TipusFigura.RECTANGLE) {
            elemfigura = doc.createElement(ET_RECTANGLE);
            escriure((Rectangle) figura, elemfigura, doc);
            arrel.appendChild(elemfigura);
        }
    }
    
     public static void escriure(Cercle c, Element cercleElem, Document doc) {
        Element elemFiguraSimple = doc.createElement(ET_FIGURA_SIMPLE);
        escriureFiguraSimple(c, elemFiguraSimple, doc);
        cercleElem.appendChild(elemFiguraSimple);
        Element nouElement = doc.createElement(ET_RADI);
        nouElement.appendChild(doc.createTextNode(String.valueOf(c.getRadi())));
        cercleElem.appendChild(nouElement);
    }
    
     public static void escriure(Rectangle r, Element rectangleElem, Document doc) {
        Element elemFiguraSimple = doc.createElement(ET_FIGURA_SIMPLE);
        escriureFiguraSimple(r, elemFiguraSimple, doc);
        rectangleElem.appendChild(elemFiguraSimple);
        Element nouElement = doc.createElement(ET_ALCADA);
        nouElement.appendChild(doc.createTextNode(String.valueOf(r.getAlcada())));
        rectangleElem.appendChild(nouElement);
        nouElement = doc.createElement(ET_AMPLADA);
        nouElement.appendChild(doc.createTextNode(String.valueOf(r.getAmplada())));
        rectangleElem.appendChild(nouElement);
    }
    
    
    protected static void escriureFiguraSimple(FiguraSimple fs, Element elementFS, Document doc){
        Element elemFigura = doc.createElement(ET_FIGURA);
        escriureFigura(fs, elemFigura, doc);
        elementFS.appendChild(elemFigura);
        Element nouElement = doc.createElement(ET_COLOR_LIN);
        
        nouElement.appendChild(doc.createTextNode(String.valueOf(fs.getColorLinia().getRGB())));
        elementFS.appendChild(nouElement);
        nouElement = doc.createElement(ET_COLOR_FIG);
       
        nouElement.appendChild(doc.createTextNode(String.valueOf(fs.getColorFigura().getRGB())));
        elementFS.appendChild(nouElement);
    }
    
     protected static void escriureFigura(Figura figura, Element elemFigura, Document doc) {
        Element elmPosicio = doc.createElement(ET_POSICIO);
        Element nouElement = doc.createElement(ET_X);
        elmPosicio.appendChild(nouElement);
        nouElement.appendChild(doc.createTextNode(String.valueOf(figura.getPosicio().x)));
        nouElement = doc.createElement(ET_Y);
        elmPosicio.appendChild(nouElement);
        nouElement.appendChild(doc.createTextNode(String.valueOf(figura.getPosicio().y)));
        elemFigura.appendChild(elmPosicio);
        nouElement = doc.createElement(ET_ESCALA);
        nouElement.appendChild(doc.createTextNode(String.valueOf(figura.getEscala())));
        elemFigura.appendChild(nouElement);
        nouElement = doc.createElement(ET_ROTACIO);
        nouElement.appendChild(doc.createTextNode(String.valueOf(figura.getRotacio())));
        elemFigura.appendChild(nouElement);
    }
     
}
