/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.FiguraSimple;
import java.awt.Point;
public class Cercle extends FiguraSimple{
        private int radi=0;

    public Cercle() {
        setTipusFigura(Modelo.TipusFigura.CERCLE);
    }

    public Cercle(Point p, int radi) {
        super(p);
        setTipusFigura(Modelo.TipusFigura.CERCLE);
        this.radi = radi;
    }

    public Cercle(int x, int y, int radi) {
        super(x, y);
        setTipusFigura(Modelo.TipusFigura.CERCLE);
        this.radi = radi;
    }

    /**
     * @return the radi
     */
    public int getRadi() {
        return radi;
    }

    /**
     * @param radi the radi to set
     */
    public void setRadi(int radi) {
        this.radi = radi;
    }
    
    @Override
    public String toString(){
    
    return super.toString()+"\t"+this.getRadi();
    
    }
    
}
