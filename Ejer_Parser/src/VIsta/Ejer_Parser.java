/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VIsta;

import Modelo.Cercle;
import Modelo.Dibuix;
import Modelo.Rectangle;
import Parser.ttlCtrlDOM;
import Parser.ttlCtrlDibujo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class Ejer_Parser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, TransformerException {
        // TODO code application logic here
        String nomFichXMl1 = "dibuix1.xml";
        File f1 = new File(nomFichXMl1);
        
        String nomFichXml2 = "dibuix2.xml";
        File f2 = new File(nomFichXml2);
        
        Document doc = null;
        ttlCtrlDibujo dibuix = new ttlCtrlDibujo();
        doc = (Document) dibuix.recuperar(f1);
        
        Dibuix dibujo = null;
        dibujo = dibuix.llegir(doc);
        
        System.out.println(dibujo);
        
        
        Dibuix dibujo2 = new Dibuix();
        Rectangle rec = new Rectangle(3,3,5,5);
        dibujo2.add(rec);
        
        Cercle cer = new Cercle(4,4,1);
        dibujo2.add(cer);
       
        Document doc2 = null;
        
        ttlCtrlDOM xtdom = new ttlCtrlDibujo();
        doc2 = xtdom.XMLaDOM();
        
        ttlCtrlDibujo xcdibu2 = new ttlCtrlDibujo();
        xcdibu2.escriure(doc2);
        xcdibu2.enmagatzemar(doc2,f2);
         System.out.println(f1);
        muestraFichero(f2);
    }
    
    
    
     static void muestraFichero(File archivo) {

        FileReader fr = null;
        BufferedReader br = null;

        try {

            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    
}
