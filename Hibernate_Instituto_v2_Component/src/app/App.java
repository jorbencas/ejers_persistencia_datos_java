/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import model.Alumno;
import model.Nombre;
import model.Profesor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
          //CREAMOS CONEXION
        //SessionFactory sessionFactory;
        //Configuration configuration = new Configuration();
        //configuration.configure();
        //sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory(); 
        
        // CREAMOS UN OBJETO
        Alumno alumno =new Alumno(7,"Jorge",45,true,Date.from(Instant.MIN),Date.from(Instant.MIN),"Hola Jorge");
        Nombre nombre = new Nombre("Pepe","Lopez","Ramirez");
        Profesor profesor = new Profesor(nombre);
        //CREAR UNA SESION
        Session session=factory.openSession();
        session.beginTransaction();
        
        //GUARDAR OBJETO
        session.save(alumno);
        session.save(profesor);
        
        //CERRAR CONEXION
        session.getTransaction().commit();
        session.close();
        factory.close();
        
        
    }
    
}