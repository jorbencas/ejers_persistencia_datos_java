/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import controlador.CtrlSax;
import java.io.FileInputStream;
import java.io.IOException;
import jdk.internal.org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author jorge
 */
public class LibroSAX {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws org.xml.sax.SAXException {
        // TODO code application logic here
        try {
            XMLReader reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler( new CtrlSax());
            reader.parse(new InputSource(new FileInputStream("Ejemplo1.xml")));
        } catch (IOException e) {
        }
    }
    
}
