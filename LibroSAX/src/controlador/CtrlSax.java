/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author jorge
 */
public class CtrlSax extends DefaultHandler {

     @Override
    public void startDocument() throws SAXException {
         System.out.println("\n Principio del docuemnto");
        //super.startDocument(); //To change body of generated methods, choose Tools | Templates.
    }
    
        @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            System.out.println("\n Processando etiqueta");
            System.out.println("\t Namespace uri" + uri);
            System.out.println("\t Nombre: "+ localName);
            System.out.println("\t Nombre con prefijo: "+ qName);
            
            System.out.println("\t Processando: " + attributes.getLength()+ " attribute...");
            for (int i = 0; i < attributes.getLength(); i++) {
                 System.out.println("\t\t Nombre: " + attributes.getQName(i));
                System.out.println("\t\t  Valor: "+ attributes.getValue(i));
            }
        //super.startElement(uri, localName, qName, attributes); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        System.out.println("\n Processando texto dentro de una etiqueta...");
        System.out.println("\t Texto: " + String.valueOf(ch, start, length));
        //super.characters(ch, start, length); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        //super.endElement(uri, localName, qName); //To change body of generated methods, choose Tools | Templates.
    }



    @Override
    public void endDocument() throws SAXException {
        System.out.println("\n Fin del documento");
        //super.endDocument(); //To change body of generated methods, choose Tools | Templates.
    }

   
    
    
    
}
