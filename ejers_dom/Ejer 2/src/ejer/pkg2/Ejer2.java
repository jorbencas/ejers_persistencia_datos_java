/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer.pkg2;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class Ejer2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
        Element agenda;
        NodeList personas;
        Node n;
        // TODO code application logic here
             DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(new File("01origen.xml"));
            
           agenda = documento.getDocumentElement();
           agenda.normalize();
           personas = agenda.getChildNodes();
           for (int i = 0; i < personas.getLength(); i++) {
            n = personas.item(i);
            System.out.println("Node" + i +"\nLocal Name" + n.getLocalName() + "El nodo Hijo" + n.getNodeValue() + "\nE l contenido" + n.getTextContent());
            }
    }
    
}
