/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer.pkg3;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class Ejer3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        // TODO code application logic here
        Element empleados;
        NodeList empleado;
        Node n;
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(new File("empleados.xml"));
          
        empleados = documento.getDocumentElement();
        empleados.normalize();
        empleado = empleados.getChildNodes();
            
           for (int i = 0; i < empleado.getLength(); i++) {
               if(empleado.item(i).getNodeType() == Node.ELEMENT_NODE){
                   leerempleado((Element) empleado.item(i));
               }
            
           }
    }
    
    public static void leerempleado( Element empleados){
        final String ET_NOMBRE = "nombre";
         final String ET_APELLIDO    = "apellidos";
         String nombre = null;
         String apellido = null;
         
         nombre = getValorEtiqueta(ET_NOMBRE, empleados);
         apellido = getValorEtiqueta(ET_APELLIDO,empleados);
         System.out.println("Nombre: \n" + nombre + "Apellidos: \n" + apellido);
    }
    
    public static String getValorEtiqueta(String etiqueta, Element element) {
         Node value = element.getElementsByTagName(etiqueta).item(0);
         return value.getChildNodes().item(0).getNodeValue();
         //return value.getChildNodes().item(0).getTextContent();
    }
    
}
