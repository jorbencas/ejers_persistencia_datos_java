/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer.pkg4.pkg2;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jorge
 */
public class Ejer42 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        // TODO code application logic here
         Element recetas;
        NodeList receta;

        final String ET_CAT = "categoria";
        final String ET_DIF = "dificultad";
         
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document Receta = builder.parse(new File("recetas2.xml"));
          
        recetas = Receta.getDocumentElement();
        recetas.normalize();
        receta = recetas.getChildNodes();
        System.out.println("--------------------------Lista de recetas------------------------");
        System.out.println("Ttitulo \t" + "Categoria \t" + "Dificultad \t" + "Descripción \t" );

        for (int i = 0; i < receta.getLength(); i++) {
            if(receta.item(i).getNodeType() == Node.ELEMENT_NODE){
                leerReceta((Element) receta.item(i));
            }
        }
    }
    public static void leerReceta ( Element receta){
        final String ET_TITULO = "titulo";
         final String ET_DIFICULTAD = "dificultad";
         final String ET_CATEGORIA = "categoria";
         final String ET_DESCRIPCIÓN = "descripcion";
         final String ET_INGREDIENTES = "ingredientes";
         
         String titulo = null;
         String dificultad = null;
         String categoria = null;
         String descripcion = null;
         String ingredientes = null;
         
         Element eIngredientes;
         
         titulo = getValorEtiqueta(ET_TITULO, receta);
         dificultad = getElementEtiqueta(ET_TITULO, receta).getAttribute(ET_DIFICULTAD);
         categoria = getElementEtiqueta(ET_TITULO, receta).getAttribute(ET_CATEGORIA);
         descripcion = getValorEtiqueta(ET_DESCRIPCIÓN, receta);
         ingredientes = getValorEtiqueta(ET_INGREDIENTES, receta);
         
         System.out.println(titulo + "\t" + dificultad + "\t" + categoria +  "\t" + descripcion +  "\t"+  ingredientes);
         
         eIngredientes = getElementEtiqueta(ET_INGREDIENTES, receta);
         NodeList ListaIngredientes = eIngredientes.getChildNodes();
         
         for (int i = 0; i < ListaIngredientes.getLength(); i++) {
               if(ListaIngredientes.item(i).getNodeType() == Node.ELEMENT_NODE){
                   System.out.println(ListaIngredientes.item(i).getTextContent());
               }
           }  
    }    
    
     public static String getValorEtiqueta(String etiqueta, Element empleados) {
         Node value = empleados.getElementsByTagName(etiqueta).item(0);
         //return value.getChildNodes().item(0).getNodeValue();
         return value.getChildNodes().item(0).getTextContent();
    }
     
     public static Element getElementEtiqueta (String etiqueta, Element empleados) {
         return (Element) empleados.getElementsByTagName(etiqueta).item(0);
    }
}
