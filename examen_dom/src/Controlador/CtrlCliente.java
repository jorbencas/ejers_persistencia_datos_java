package Controlador;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import Modelo.Cliente;
import Modelo.Constants;
import Modelo.Factura;

public class CtrlCliente  extends CtrlDom {

	public static Cliente llegirClient (Element elemcliente){
		Cliente client = new Cliente();
		Element Factura = getElemento(Constants.ET_FACTURA, elemcliente);
		client.setCategoria(getAtributoEtiqueta(Constants.ET_CATEGORIA, elemcliente));
		Element nombre = getElemento(Constants.ET_NOMBRE, elemcliente);
		client.setNombre(getValorEtiqueta(Constants.ET_NOMBRE, elemcliente));		;
		client.setApellidos(getValorEtiqueta(Constants.ET_APELLIDOS, elemcliente));
		
		client.setLang(getAtributoEtiqueta(Constants.ET_LANG,nombre));
		llegirFactura(client,Factura);
		return client;
	}
	
	public static void llegirFactura(Cliente client,Element Factura){
		Factura factura = new Factura();
		factura.setTipo(getAtributoEtiqueta(Constants.ET_TIPO,Factura));
		factura.setFecha(getValorEtiqueta(Constants.ET_FECHA, Factura));
		factura.setTotal(getValorEtiqueta(Constants.ET_TOTAL, Factura));
		client.setFactura(factura);
	}
	
	public static void escriure(Cliente client, Document doc, Element Empresa){
		Element cliente = doc.createElement(Constants.ET_CLIENTE);
		Element nombre = doc.createElement(Constants.ET_NOMBRE);
		nombre.appendChild(doc.createTextNode(client.getNombre()));
		cliente.appendChild(nombre);
		
		Attr categoria = doc.createAttribute(Constants.ET_CATEGORIA);
		categoria.setNodeValue(client.getCategoria());
		cliente.setAttributeNode(categoria);
		
		Attr lang = doc.createAttribute(Constants.ET_LANG);
		lang.setNodeValue(client.getLang());
		nombre.setAttributeNode(lang);
		
		Element apellidos = doc.createElement(Constants.ET_APELLIDOS);
		apellidos.appendChild(doc.createTextNode(client.getApellidos()));
		cliente.appendChild(apellidos);
		
		excriurefactura(client,doc,cliente);
		Empresa.appendChild(cliente);
	}
	
	public static void excriurefactura(Cliente client, Document doc, Element Cliente){
		Factura Factura = client.getFactura();
		Element factura = doc.createElement(Constants.ET_FACTURA);
		
		Attr kind = doc.createAttribute(Constants.ET_TIPO);
		kind.setNodeValue(Factura.getTipo());
		factura.setAttributeNode(kind);

		Element fecha = doc.createElement(Constants.ET_FECHA);
		fecha.appendChild(doc.createTextNode(Factura.getFecha()));
		factura.appendChild(fecha);
		
		Element total = doc.createElement(Constants.ET_TOTAL);
		total.appendChild(doc.createTextNode(Factura.getTotal()));
		factura.appendChild(total);
		
		Cliente.appendChild(factura);
	}
}
