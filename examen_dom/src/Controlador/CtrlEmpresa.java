package Controlador;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import Modelo.Cliente;
import Modelo.Constants;
import Modelo.Empresa;
import Modelo.Factura;

public class CtrlEmpresa extends CtrlDom {
	private File file = null;
	private Empresa empresa  = null;
	
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public CtrlEmpresa() {
		empresa = new Empresa();
	}

	public CtrlEmpresa(File file, Empresa empresa) {
		this.file = file;
		this.empresa = empresa;
	}
	
	public Document recuperar(File f1) throws ParserConfigurationException, SAXException, IOException{
		Document doc = null;
		doc = CtrlDom.XMLaDOM(f1);
		return doc;
	}
	
	public Document recuperar() throws ParserConfigurationException, SAXException, IOException{
		Document doc = CtrlDom.XMLaDOM();
		return doc;
	}
	
	
	
	public Empresa llegir(Document doc){
		Element elem_empresa = doc.getDocumentElement();
		NodeList listaclientes = elem_empresa.getChildNodes();
		
		for (int i = 0; i < listaclientes.getLength(); i++) {
			if(listaclientes.item(i).getNodeType() == Node.ELEMENT_NODE){
				empresa.add(CtrlCliente.llegirClient((Element) listaclientes.item(i)));
			}
		}

		return empresa;
		
	}
	
	public void enmagatzemar(Document doc,File f1) throws TransformerConfigurationException, TransformerException{
		CtrlDom.escribirArchivoXml(doc,f1);
	}
	
	public void escriure(Document doc, Empresa emp){
		Element raiz = doc.createElement(Constants.ET_EMPRESA);
		for (Cliente f : emp) {
			CtrlCliente.escriure(f,doc,raiz);
		}
		
		doc.appendChild(raiz);
	}
	
	public ArrayList<Cliente> EmpresasdeEjemplo(ArrayList<Cliente> empresa){

		Cliente client1 = new Cliente("Primero","Jorge","en","Beneyto",new Factura("C","12/20/2004","300"));
		empresa.add(client1);
		Cliente client2 = new Cliente("Segundo", "Pascual","es","Romero",new Factura("A","12/20/1996","543"));
		empresa.add(client2);
		Cliente client3 = new Cliente("Tercero","Antonia","en","Gomez",new Factura("B","12/20/2008","432"));
		empresa.add(client3);
		return empresa;
	}
	
	
}
