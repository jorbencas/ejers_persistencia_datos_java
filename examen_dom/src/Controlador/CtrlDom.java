/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author mati
 */
public class CtrlDom {
    
    public static Document XMLaDOM() throws ParserConfigurationException{
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        return doc;
    }
    
    public static Document XMLaDOM(File xml) throws ParserConfigurationException, SAXException, IOException{
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xml);
        doc.getDocumentElement().normalize();
        return doc;
    }
    
    public static void escribirArchivoXml(Document doc, File xml) throws TransformerConfigurationException, TransformerException{
        Transformer trans = TransformerFactory.newInstance().newTransformer();
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        
        
        StreamResult stream = new StreamResult(xml);
        DOMSource dom = new DOMSource(doc);
        trans.transform(dom, stream);
    }
    
    public static String getValorEtiqueta(String etiqueta, Element elemento){
        Node nodo = elemento.getElementsByTagName(etiqueta).item(0);
        return nodo.getChildNodes().item(0).getNodeValue();
    }
    
    public static Element getElemento(String etiqueta, Element elemento){
        return (Element) elemento.getElementsByTagName(etiqueta).item(0);
    }
    
    public static String getAtributoEtiqueta(String nomAtributo, Element elem) {
		return elem.getAttribute(nomAtributo);
	}
    
}
