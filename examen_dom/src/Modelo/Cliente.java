package Modelo;

public class Cliente {
	private String  categoria = "";
	private String nombre = "";
	private String lang = "";
	private String apellidos = "";
	private Factura factura = new Factura();
	
	public Cliente() {
		// TODO Auto-generated constructor stub
	}

	public Cliente( String categoria, String nombre, String lang, String apellidos, Factura factura) {
		this.categoria = categoria;
		this.nombre = nombre;
		this.lang = lang;
		this.apellidos = apellidos;
		this.factura = factura;
	}

	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	@Override
	public String toString() {
		return "\n Cliente [ Categoria: " + this.categoria  + " nombre=" + this.nombre + ", lang=" + this.lang + ", apellidos=" + this.apellidos + ", factura=" + this.factura.toString()
				+ "]";
	}
	
	
}
