package Modelo;

public class Factura {
	private String tipo = "";
	private String fecha = "";
	private String total = "";
	
	public Factura() {
	}
	
	public Factura(String Tipo,String fecha, String total) {
		this.tipo = Tipo;
		this.fecha = fecha;
		this.total = total;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Factura [ Tipo " + tipo + " fecha=" + fecha + ", total=" + total + "]";
	}
	
	
}
