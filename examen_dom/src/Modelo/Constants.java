package Modelo;

public class Constants {
	public static String ET_EMPRESA = "empresa";
	public static String ET_CLIENTE = "cliente";
	public static String ET_CATEGORIA = "categoria";
	public static String ET_NOMBRE = "nombre";
	public static String ET_LANG = "lang";
	public static String ET_APELLIDOS = "apellidos";
	public static String ET_FACTURA = "factura";
	public static String ET_TIPO = "tipo";
	public static String ET_FECHA = "fecha";
	public static String ET_TOTAL = "total";
	
}
