package Vista;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


import Controlador.CtrlDom;
import Controlador.CtrlEmpresa;
import Modelo.Empresa;

public class main {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {

		int opcion = 777;
		String ruta = "";
		File f2 = new File("empresa_Jorge_Beneyto_Castello.xml");
		CtrlEmpresa emp = new CtrlEmpresa();
		Empresa empresainstancia = new Empresa();
		Empresa empresainstancia2 = new Empresa();
		
		Document doc = null;
		Document doc2 = null;        
		Scanner teclado = new Scanner(System.in);
		while (opcion != 0) {
			mostrarMenu();
			opcion = teclado.nextInt();
			teclado.nextLine();
			switch (opcion) {
				case 1:
					System.out.println("Introduzca ruta del fichero xml o dejalo en blanco en tal caso se utilizara la ruta por defecto");
					ruta = teclado.nextLine();
					if (ruta.length() == 0) {
						ruta = "empresa.xml";
					}
					 doc = (Document) emp.recuperar(new File(ruta));
					break;

				case 2:
					empresainstancia = emp.llegir(doc);
					break;

				case 3:
					System.out.println(empresainstancia);
					break;

				case 4:
					Document doc2 = (Document) emp.recuperar();
					emp.EmpresasdeEjemplo(empresainstancia2);
					emp.escriure(doc2, empresainstancia2 );
					break;
				case 5:
					emp.enmagatzemar(doc2, f2);
					break;
			}
		}
	}

	static void menu(){
		System.out.println("1.- Seleccionar un fichero xml a recuperar y crea un document");
        System.out.println("2.- Lee del documento y crea un objeto Resultado");
        System.out.println("3.-Muestra el objeto Resultado");
        System.out.println("4.-Crea un objeto resultado dando de alta un par de locales con sus licencias y escribe en un documento vacio ");
        System.out.println("5.-Guardado en copia.xml");
	}
}
