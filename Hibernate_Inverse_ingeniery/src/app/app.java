package app;

import conexion.AlumnoDAO;
import java.time.Instant;
import java.util.Date;
import modelo.Alumno;

/**
 *
 * @author jorge
 */
public class app {
    
    public static void main (String[] args) {
        AlumnoDAO d = new AlumnoDAO();
        
        //Alta alumno
        Alumno a = new Alumno(5, "Francisco", 35, "Alta Alumno", "eS UN BUEN ESTUDIANTE");
        d.altaAlumno(a);
        
        // Cosulta Alumno
        Alumno a2 = d.obtenrAlumn(2);
        System.out.println("Codigo: " + a2.getId());
        System.out.println("Nombre: " + a2.getNombre());
        
        //Baja Alumno
        Alumno a3 = new Alumno();
        a3.setId(3);
        d.bajaAlumno(a3);
    }
}
