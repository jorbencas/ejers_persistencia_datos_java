/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import modelo.Alumno;
import org.hibernate.Session;



/**
 *
 * @author jorge
 */
public class AlumnoDAO {
    
    public Alumno obtenrAlumn(int id){
        Session s = HibernateUtil.getCurrrentSession();
        Alumno result = (Alumno) s.get(Alumno.class, id);
        s.getTransaction().commit();
        s.close();
        return result;
    }
    
    public  void altaAlumno(Alumno a){
        Session s = HibernateUtil.getCurrrentSession();
        s.beginTransaction();
        s.save(a);
        s.getTransaction().commit();
        s.close();
    }
    
    public void bajaAlumno(Alumno a){
        Session s = HibernateUtil.getCurrrentSession();
        s.beginTransaction();
        s.delete(a);
        s.getTransaction().commit();
        s.close();
    }
}
