/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 *
 * @author jorge
 */
public class Cliente_DAO {
    public void crear(Connection con, Cliente cliente) throws Exception{
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT INTO cliente VALUES (?,?,?,?,?,?,?,?)");
            stmt.setInt(1, cliente.getDNI());
            stmt.setString(2, cliente.getNombre());
            System.out.println("nombre cliente"+ cliente.getNombre());
            stmt.setString(3, cliente.getApellido1());
            stmt.setString(4, cliente.getApellido2());
            stmt.setString(5, cliente.getNick());
            stmt.setString(6, cliente.getPassword());
            stmt.setFloat(7, cliente.getSaldo());
            System.out.println("Saldo cliente"+ cliente.getSaldo());
            if(cliente.isMoroso() == true){
                stmt.setInt(8, 1);            
            }
            else{
                stmt.setInt(8, 0);
            }     
             System.out.println("Moroso cliente"+ cliente.isMoroso());
            stmt.execute();
            
        }catch (SQLException ex) {
            ex.printStackTrace();
            throw  new Exception("Problemas al actualizar cliente "+ex.getMessage());
        } finally{
            if(stmt!=null){
                stmt.close();
            }
        }
    }
    
    public void actualitza(Connection con, Cliente cliente) throws Exception{
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE cliente SET Nombre=?, Ape1=?, Ape2=?, Nick=?, Passwd=?, Saldo=?, Moroso=? WHERE DNI=?");
            stmt.setString(1, cliente.getNombre());
            System.out.println("nombre cliente"+ cliente.getNombre());
            stmt.setString(2, cliente.getApellido1());
            stmt.setString(3, cliente.getApellido2());
            stmt.setString(4, cliente.getNick());
            stmt.setString(5, cliente.getPassword());
            stmt.setFloat(6, cliente.getSaldo());

            if(cliente.isMoroso()){
                stmt.setInt(7, 1);            
            }
            else{
                stmt.setInt(7, 0);
            }     
            stmt.setInt(8, cliente.getDNI());
            stmt.execute();
            
        }catch (SQLException ex) {
            ex.printStackTrace();
            throw  new Exception("Problemas al actualizar cliente "+ex.getMessage());
        } finally{
            if(stmt!=null){
                stmt.close();
            }
        }
    }
    
    
    public void borrar(Connection con, Cliente cliente) throws Exception{
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM cliente WHERE DNI LIKE ? ");
            stmt.setInt(1, cliente.getDNI());
            stmt.execute();
            
        }catch (SQLException ex) {
            ex.printStackTrace();
            throw  new Exception("Problemas al actualizar cliente "+ex.getMessage());
        } finally{
            if(stmt!=null){
                stmt.close();
            }
        }
    }
}
