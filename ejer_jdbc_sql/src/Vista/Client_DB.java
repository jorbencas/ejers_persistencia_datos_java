/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import DAO.Cliente_DAO;
import DAO.ConexionaDB;
import Entity.Cliente;
import java.sql.Connection;

/**
 *
 * @author jorge
 */
public class Client_DB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            ConexionaDB conexion_DB = new ConexionaDB();
            System.out.println("Abrir conexion");
            Connection con = conexion_DB.abrirconexion();
            System.out.println("conexion abierta");
            
            Cliente_DAO clientedao = new Cliente_DAO();
            
            Cliente clienteinsert = new Cliente(88,"Alex","Ramiro","gines","Aleragi","4022");
            clientedao.crear(con, clienteinsert);
            
            Cliente clienteupdate = new Cliente(14,"Andres","Ramiro","Pajares","Andrapa","1234");
            clienteupdate.setMoroso(true);
            clientedao.actualitza(con, clienteupdate);
            
            Cliente clientedelete = new Cliente(89,"Alex","Ramiro","gines","perico","4022");
            clientedao.borrar(con, clientedelete);
            System.out.println("cerrar conexión");
            conexion_DB.cerrarconexion(con);
            System.out.println("Conexión cerrada");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
