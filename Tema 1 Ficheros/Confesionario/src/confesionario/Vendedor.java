/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confesionario;

import java.util.ArrayList;
import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class Vendedor implements Serializable {
     private int codigo;
    private String nombre,categoria;
    ArrayList<Vehiculo> listaCochesVendidos;

    public Vendedor(int codigo, String nombre, String categoria) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.categoria = categoria;
        this.listaCochesVendidos=new ArrayList<Vehiculo>();
    }

    @Override
    public String toString() {
        //si queremos imprimir el listado de ventas del vendedor,añadiríamos esto
        //"listado coches vendidos" + this.listaCochesVendidos+
        //no lo incluyo porque lo muestro mediante un case y un método
        return "Vendedor{" + "codigo=" + codigo + ", nombre=" + nombre + ", categoria=" + categoria+ '}';
    }
    
    public void añadirVenta(Vehiculo v){
        listaCochesVendidos.add(v);
    }

    public void mostrarVentas(){
        for (Vehiculo venta : listaCochesVendidos) {
            System.out.println(venta);
        }
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public ArrayList<Vehiculo> getListaCochesVendidos() {
        return listaCochesVendidos;
    }

    public void setListaCochesVendidos(ArrayList<Vehiculo> listaCochesVendidos) {
        this.listaCochesVendidos = listaCochesVendidos;
    }
}
