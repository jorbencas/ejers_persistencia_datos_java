/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confesionario;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Confesionario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner teclado = new Scanner(System.in);

        String matricula, modelo, marca, color, nombre, categoria;
        int potencia, codigo, opcion = 0, precio;
        Vehiculo veh;
        Vendedor ven;
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
        ArrayList<Vendedor> vendedores = new ArrayList<Vendedor>();

        do {

            System.out.println("Elige una opción");
            menu();
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1:
                    //ALTA VEHICULOS
                    System.out.println("Introduce la matrícula del vehículo");
                    teclado.nextLine();
                    matricula = teclado.nextLine();

                    System.out.println("Introduce el modelo del vehículo");
                    modelo = teclado.nextLine();

                    System.out.println("Introduce la marca del vehículo");
                    marca = teclado.nextLine();

                    System.out.println("Introduce la potencia del vehículo");
                    potencia = teclado.nextInt();

                    System.out.println("Introduce el color del vehículo");
                    teclado.nextLine();
                    color = teclado.nextLine();

                    System.out.println("Introduce el precio del vehículo");
                    precio = teclado.nextInt();

                    //CREAMOS OBJETO DE LA CLASE
                    veh = new Vehiculo(marca, modelo, matricula, color, potencia, precio);
                    //AÑADIMOS AL ARRAYLIST
                    vehiculos.add(veh);

                    break;
                case 2:
                    //ALTA VENDEDORES  

                    System.out.println("Introduce el codigo del vendedor");
                    codigo = teclado.nextInt();

                    System.out.println("Introduce el nombre del vendedor");
                    teclado.nextLine();
                    nombre = teclado.nextLine();

                    System.out.println("Introduce la categoría del vendedor");
                    categoria = teclado.nextLine();

                    //CREAMOS OBJETO DE LA CLASE
                    ven = new Vendedor(codigo, nombre, categoria);
                    //AÑADIMOS AL ARRAYLIST
                    vendedores.add(ven);

                    break;
                case 3:
                    //MOSTRAMOS VENDEDORES
                    verVendedores(vendedores);
                    break;
                case 4:
                    //MOSTRAMOS VEHICULOS
                    verVehiculos(vehiculos);
                    break;
                case 5:
                    //REGISTRAR VENTA
                    System.out.println("Introduce el código del vendedor que va a realizar la venta");
                    verVendedores(vendedores);
                    codigo = teclado.nextInt();
                    ven = buscarVendedor(vendedores, codigo);
                    if (ven == null) {
                        System.out.println("Vendedor no encontrado");
                    } else {
                        System.out.println("Vendedor encontrado");
                        System.out.println(ven);

                        System.out.println("Introduce la matrícula del vehículo a vender");
                        verVehiculos(vehiculos);
                        teclado.nextLine();
                        matricula = teclado.nextLine();
                        veh = buscarVehiculo(vehiculos, matricula);
                        if (veh == null) {
                            System.out.println("Vehículo no encontrado");
                        } else {
                            System.out.println("Vehículo encontrado");
                            System.out.println(veh);

                            ven.añadirVenta(veh);
                            System.out.println("Venta añadida al vendedor seleccionado");
                            System.out.println(ven);
                            ven.mostrarVentas();
                        }
                    }
                    break;
                case 6:
                    //VER VENTAS DE UN VENDEDOR

                    //MOSTRAMOS LISTADO DE VENDEDORES
                    System.out.println("Mostrando vendedores");
                    verVendedores(vendedores);
                    System.out.println("Introduce el código del vendedor del cuál consultar las ventas");
                    codigo = teclado.nextInt();
                    ven = buscarVendedor(vendedores, codigo);
                    if (ven == null) {
                        System.out.println("Vendedor no encontrado");
                    } else {
                        System.out.println("Vendedor encontrado. MOSTRANDO SU LISTA DE VENTAS...");
                        ven.mostrarVentas();
                    }
                    break;

                case 7:
                    //GUARDAR LOS DATOS DE LOS VENDEDORES Y LOS VEHICULOS EN FICHEROS

                    //----------------------GUARDAMOS LOS VEHICULOS----------------
                    File f = new File("vehiculos.dat");
                    //si el archivo existe es que tiene contenido (al guardar contenido se crea), 
                    //si existe usamos la clase modificada para no escribir cabecera
                    //si no existe, usamos el metodo normal que escribe la cabecera
                    if (f.exists()) {
                        MiObjectOutputStream salida = new MiObjectOutputStream(new FileOutputStream(f, true));
                        guardarVehiculos(salida, vehiculos);

                        salida.close();
                    } else {
                        ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(f));

                        guardarVehiculos(salida, vehiculos);

                        salida.close();
                    }

                    //----------------------GUARDAMOS LOS VENDEDORES----------------
                    File f2 = new File("vendedores.dat");
                    if (f2.exists()) {
                        //ESCRIBE A PARTIR DE LA SEGUNDA VEZ, CON TRUE (PARA PODER ESCRIBIR EL RESTO, SIN BORRARLO TODO)
                        MiObjectOutputStream salida2 = new MiObjectOutputStream(new FileOutputStream(f2, true));

                        guardarVendedores(salida2, vendedores);

                        salida2.close();
                    } else {
                        //ESCRIBE POR PRIMERA VEZ, SIN TRUE (SOLO ESCRIBE UN OBJETO)
                        ObjectOutputStream salida2 = new ObjectOutputStream(new FileOutputStream(f2));

                        guardarVendedores(salida2, vendedores);

                        salida2.close();
                    }

                    System.out.println("Datos añadidos con éxito a los ficheros");

                    break;
                case 8:
                    //LEER LOS FICHEROS BINARIOS

                    System.out.println("LEYENDO FICHERO VEHICULOS.DAT");

                    try {
                        ObjectInputStream entrada = new ObjectInputStream(new FileInputStream("vehiculos.dat"));

                        Vehiculo veh2;
                        //MIENTRAS HAYA OBJETOS, SIGUE LEYENDO
                        while ((veh2 = (Vehiculo) entrada.readObject()) != null) {
                            System.out.println(veh2);

                        }
                        entrada.close();
                        //AL FINALIZAR EL FICHERO, EL READOBJECT LANZA UNA EXCEPCIÓN, QUE CAPTURAMOS AQUÍ
                    } catch (EOFException e) {
                        System.out.println("Fin del fichero");
                    }

                    System.out.println("LEYENDO FICHERO VENDEDORES.DAT");

                    try {
                        ObjectInputStream entrada2 = new ObjectInputStream(new FileInputStream("vendedores.dat"));

                        Vendedor ven2;
                        //MIENTRAS HAYA OBJETOS, SIGUE LEYENDO
                        while ((ven2 = (Vendedor) entrada2.readObject()) != null) {
                            System.out.println(ven2);
                            ven2.mostrarVentas();

                        }
                        entrada2.close();
                        //AL FINALIZAR EL FICHERO, EL READOBJECT LANZA UNA EXCEPCIÓN, QUE CAPTURAMOS AQUÍ
                    } catch (EOFException e) {
                        System.out.println("Fin del fichero");
                    }

                    break;

                case 9:
                    System.out.println("Saliendo..");
                default:
                    System.out.println("Opción inválida");
                    break;
            }

        } while (opcion
                != 0);

    }

    public static void menu() {
        System.out.println("1. Alta vehiculos");
        System.out.println("2  Alta vendedores");
        System.out.println("3. Ver vendedores");
        System.out.println("4. Ver vehículos");
        System.out.println("5. Registrar la venta de un vehiculo por un vendedor");
        System.out.println("6. Ver ventas de un vendedor");
        System.out.println("7. Guardar los datos de los vendedores y de los vehículos en ficheros");
        System.out.println("8  Leer los ficheros binarios");
        System.out.println("9. Salir");
    }

    public static void verVendedores(ArrayList<Vendedor> vendedores) {
        for (Vendedor v : vendedores) {
            System.out.println(v);
        }
    }

    public static void verVehiculos(ArrayList<Vehiculo> vehiculos) {
        for (Vehiculo v : vehiculos) {
            System.out.println(v);
        }
    }

    public static Vendedor buscarVendedor(ArrayList<Vendedor> vendedores, int codigo) {
        for (Vendedor v : vendedores) {
            if (v.getCodigo() == codigo) {
                return v;
            }
        }
        return null;
    }

    public static Vehiculo buscarVehiculo(ArrayList<Vehiculo> vehiculos, String matricula) {
        for (Vehiculo v : vehiculos) {
            if (v.getMatricula().equalsIgnoreCase(matricula)) {
                return v;
            }
        }
        return null;
    }

    public static void guardarVehiculos(ObjectOutputStream salida, ArrayList<Vehiculo> vehiculos) throws IOException {
        for (Vehiculo v : vehiculos) {

            salida.writeObject(v);
        }
        vehiculos.clear();
    }

    public static void guardarVendedores(ObjectOutputStream salida2, ArrayList<Vendedor> vendedores) throws IOException {
        for (Vendedor v : vendedores) {

            salida2.writeObject(v);
        }
        vendedores.clear();
    }
    
}
