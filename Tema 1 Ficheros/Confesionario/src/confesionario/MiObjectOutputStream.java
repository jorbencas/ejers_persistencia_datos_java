/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confesionario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
/**
 *
 * @author jorge
 */
public class MiObjectOutputStream  extends ObjectOutputStream {
     //Constructor que recibe ObjectOutputStream
    public MiObjectOutputStream(OutputStream salida) throws IOException
    {
        super(salida);
    }

    //Constructor sin parámetros
    protected MiObjectOutputStream() throws IOException, SecurityException
    {
        super();
    }

    //Reescribimos el método de escribir para que no escriba bytes de cabecera, así se escribirá a continuación
    //del anterior y se podrá leer al cerrar el programa con un único ObjectOutputStream
    protected void writeStreamHeader() throws IOException
    {
    }
}
