/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import model.PilaJuego;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge
 */
public class GestionFicheros {
     File file;
    RandomAccessFile fin;
        
    public GestionFicheros(File file) {
        setFile(file);
    }
    @Override
    public void finalize() throws IOException, Throwable{
        this.fin.close();
        super.finalize();
    }
    
    public void setFile(File file){
    this.file=file;
   
        try {
            this.file.deleteOnExit();
            this.file.createNewFile();
            fin=new RandomAccessFile(file,"rw");
        } catch (IOException ex) {
            Logger.getLogger(PilaJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
//Funcción para crear y añadir elementos a la pila    
    public void empilar (PilaJuego estat){
        ByteArrayOutputStream bOut=new ByteArrayOutputStream();
        ObjectOutputStream oOut;
        
        System.out.println(estat);
        
        try {
            oOut=new ObjectOutputStream(bOut);
             oOut.writeObject(estat);
             byte[] bArray=bOut.toByteArray();
             fin.seek(fin.length());
             fin.write(bArray);
             fin.writeInt(bArray.length);
             
        } catch (IOException ex) {
            Logger.getLogger(PilaJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
//Función para recuperar y revertir el orden de salida de la pila    
    public PilaJuego desempilar() {
        PilaJuego estat=null;
        int numBytes;
        
        try {
            fin.seek(fin.length()-4);
            numBytes=fin.readInt();
            fin.seek(fin.length()-4-numBytes);
            byte[] bArray=new byte[numBytes];
            fin.readFully(bArray);
            ObjectInputStream oIn=new ObjectInputStream(new ByteArrayInputStream(bArray));      
            estat=(PilaJuego) oIn.readObject();
            fin.setLength(fin.length()-4-numBytes);
        } catch (IOException ex) {
            Logger.getLogger(PilaJuego.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PilaJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estat;
    }
    
    public boolean esBuit(){
    
        try {
            return fin.length()==0;
        } catch (IOException ex) {
            Logger.getLogger(PilaJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        }  
    
    public void eliminarFichero() {

            if (!this.file.exists()) {
             System.out.println("El archivo data no existe.");
            } else {
                this.file.delete();
                System.out.println("El archivo data fue eliminado.");
            }
    }

}
