/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import model.Jugadores;
import model.PilaJuego;
import static Vista.Juego3enraya_Jorge_Beneyto_Castelló.menuconfirmar;
import static Vista.Juego3enraya_Jorge_Beneyto_Castelló.submenu;
import java.io.*;
import java.util.*;

/**
 *
 * @author jorge
 */
public class GestionarJuego {
    GestionFicheros gf = new GestionFicheros(new File("3enraya.obj"));
    char tablero [][] = new char [3][3];
    char jugador1 = 'X';
    char jugador2 = 'O';
    boolean winner = false;
    int contador = 0;
    Jugadores Gamer = null; 
    PilaJuego pila = null;
    

//Inicializamos el tablero   
    public void inicialitzartablero(){
        this.contador = 0;

        for (int i=0;i<this.tablero.length;i++){
            System.out.print(" | ");
            for (int j=0;j<this.tablero[i].length;j++){
                this.tablero[i][j]='-';
                System.out.print( this.tablero[i][j] + " | ");
            }
             System.out.println( "\n---------------");
        }
    }
  
//comprobamos que el valor introducido es valido
    public boolean pedirvalor( int i, int j, char jugador){
        boolean valido = false;

        if((i<0) || (i>2) || (j<0) || (j>2)){
            System.out.println("Numero no valido");
        }else{
            if( this.tablero[i][j] == jugador  ){ 
                System.out.print("No se puede poner ahi la ficha");
                valido = false;
            }else
                valido = true;
        }
        
        return valido;
    }
  
//Cambiamos el tablero en función de las jugadas
    public void changetablero(){
       Scanner sn = new Scanner(System.in);
       int i,j, w, q, Gamer;

       do{
         if( this.contador > 6 ){
            desplazarficha();
         }else{
            do{
                System.out.print("Jugador 1 selecciona fila: ");
                i =  sn.nextInt();
                System.out.print("Jugador 1 selecciona columna: ");
                j = sn.nextInt();
            }while(pedirvalor(i,j,'O') != true);

            Gamer = 1;
           
            drawtablero(i,j, this.jugador1);
            this.contador++;
            System.out.println(this.contador);
            opciónjugo(Gamer);
            System.out.println(this.contador);
                    
            if( this.winner != true  ){
                do{
                    System.out.print("Jugador 2 selecciona fila: ");
                    w =  sn.nextInt();
                    System.out.print("Jugador 2 selecciona columna: ");
                    q = sn.nextInt();
                }while(pedirvalor(w,q,'X') != true);
             
                Gamer = 2;

                drawtablero(w,q, this.jugador2);
                this.contador++;
                System.out.println(this.contador);
                opciónjugo(Gamer);

            }
         }
        }while(comprobartablero() != true);
    }
  
 //Submenu para elegir si queremos continuar jugando o volver atras
    public void opciónjugo(int Game1){
        Scanner tec = new Scanner(System.in);
        int option = 0;
        
        submenu();
        System.out.println("Elige una opción: ");
        option = tec.nextInt();
        
        if(option == 2) {
            Deshacerjugada();
        }else{
            this.Gamer = new Jugadores(Game1,this.contador,this.winner);
            this.contador = this.Gamer.getTurno();
            this.winner = this.Gamer.isGanador();
            this.pila = new PilaJuego(this.tablero,this.Gamer);
            gf.empilar(this.pila);
        };                                  
    }
 
 //Ver Jugada/s anteriores
    public void Deshacerjugada(){
        Scanner tec = new Scanner(System.in);
        int option = 0;
        boolean returnagain = false;
        
        do{
            if(!gf.esBuit()){
               this.pila = gf.desempilar();
                System.out.println(this.pila); 
            }

            Jugadores jugadortmp = pila.getJugador();

            for ( int x = 0; x < this.pila.getTablero().length; x++ ){
                System.out.print(" | ");
                    for ( int y = 0; y < this.pila.getTablero()[0].length; y++ ){
                        System.out.print( this.pila.getTablero()[x][y] + " | ");
                    }
                System.out.println( "\n---------------");
            }  
            
               this.Gamer = new Jugadores(jugadortmp.getRole(), jugadortmp.getTurno() -1,this.winner);
             
//Menu para comprobar si el usuaquiere seguir deshaciendo jugadas, o quiere seguir jugando
            menuconfirmar();
            System.out.println("Introduce el numero de la pieza: ");
            option = tec.nextInt();
            switch(option){
                case 1:
                    returnagain = false;
                    break;
                case 2:
                    returnagain = true;
                    break;
                }
        }while(returnagain != true);
        
        
        this.contador = this.Gamer.getTurno();
        this.winner = this.Gamer.isGanador();
        this.pila = new PilaJuego(this.tablero,this.Gamer);
        gf.empilar(this.pila);
            
    }
  
 //Función para cuando se tiene 3 fichas y no se ha hecho 3 en raya, por tanto se debe cambiar la ficha, hasta conseguir un 3 en raya
    public void desplazarficha(){
       Scanner sn = new Scanner(System.in);
       int i,j, w, q, Gamer, pieza, contpieza1 = 0, contpieza2 = 0;
       
       System.out.println("Introduce el numero de la pieza: ");
       pieza = sn.nextInt();
       
        for ( int a = 0; a < this.tablero.length; a++ ){
            for ( int b = 0; b < this.tablero[0].length; b++ ){ 
                if( this.tablero[a][b] == this.jugador1 ){
                    contpieza1++;
                    if(pieza == contpieza1){
                        this.tablero[a][b] = '-';
                    }
                }
            }
        }
       
        do{
            System.out.print("Jugador 1 selecciona fila donde la quieres mover: ");
            i =  sn.nextInt();
            System.out.print("Jugador 1 selecciona columna donde la quieres mover: ");
            j = sn.nextInt();
        }while(pedirvalor(i,j, 'O') != true);
       
            Gamer = 1;
           
            drawtablero(i,j, this.jugador1);
            
             if( this.Gamer.isGanador() != true ){
                opciónjugo(Gamer);
             }
            if( this.Gamer.isGanador() != true ){
                System.out.println("Introduce el numero de la pieza: ");
                pieza = sn.nextInt();
       
                for ( int a = 0; a < this.tablero.length; a++ ){
                    for ( int b = 0; b < this.tablero[0].length; b++ ){ 
                        if( this.tablero[a][b] == this.jugador2 ){
                            contpieza2++;
                            if(pieza == contpieza2)
                                this.tablero[a][b] = '-';
                        }
                    }
                }
            
                do{
                    System.out.print("Jugador 2 selecciona fila donde la quieres mover: ");
                    w =  sn.nextInt();
                    System.out.print("Jugador 2 selecciona columna donde la quieres mover: ");
                    q = sn.nextInt();
                }while( pedirvalor(w,q, 'X') != true);
             
                Gamer = 2;
                
               drawtablero(w,q, this.jugador2);
               
               opciónjugo(Gamer);
            }      
    }

//Funcción para saber el jugador que ha ganado    
    public boolean comprobartablero(){
         
            if ((this.tablero[0][0] == this.jugador1) && (this.tablero[0][1] == this.jugador1) && (this.tablero[0][2]== this.jugador1)){
		this.winner = true;
                System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[1][0] == this.jugador1) && (this.tablero[1][1] == this.jugador1) && (this.tablero[1][2]==this.jugador1)){
		this.winner = true;
		System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[2][0] == this.jugador1) && (this.tablero[2][1] == this.jugador1) && (this.tablero[2][2]==this.jugador1)){
                this.winner = true;
		System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[0][0] == this.jugador1) && (this.tablero[1][1] == this.jugador1) && (this.tablero[2][2]==this.jugador1)){
                this.winner = true;
                System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[0][2] == this.jugador1) && (this.tablero[1][1] == this.jugador1) && (this.tablero[2][0]==this.jugador1)){
                this.winner = true;
		System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[0][0] == this.jugador1) && (this.tablero[1][0] == this.jugador1) && (this.tablero[2][0]==this.jugador1)){
		this.winner = true;
		System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[0][1] == this.jugador1) && (this.tablero[1][1] == this.jugador1) && (this.tablero[2][1]==this.jugador1)){
                this.winner = true;
                System.out.println("¡Jugador 1 gana!");
            }
            if ((this.tablero[0][2] == this.jugador1) && (this.tablero[1][2] == this.jugador1) && (this.tablero[2][2]==this.jugador1)){
                this.winner = true;
		System.out.println("¡Jugador 1 gana!");
            }
                        
                        
            //JUGADOR 2:
 
            if ((this.tablero[0][0] == this.jugador2) && (this.tablero[0][1] == this.jugador2) && (this.tablero[0][2]==this.jugador2)){
		this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[1][0] == this.jugador2) && (this.tablero[1][1] == this.jugador2) && (this.tablero[1][2]==this.jugador2)){
		this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[2][0] == this.jugador2) && (this.tablero[2][1] == this.jugador2) && (this.tablero[2][2]==this.jugador2)){
		this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[0][0] == this.jugador2) && (this.tablero[1][1] == this.jugador2) && (this.tablero[2][2]==this.jugador2)){
		this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[0][2] == this.jugador2) && (this.tablero[1][1] == this.jugador2) && (this.tablero[2][0]==this.jugador2)){
		this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[0][0] == this.jugador2) && (this.tablero[1][0] == this.jugador2) && (this.tablero[2][0]==this.jugador2)){
                this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[0][1] == this.jugador2) && (this.tablero[1][1] == this.jugador2) && (this.tablero[2][1]==this.jugador2)){
                this.winner = true;
		System.out.println("¡Jugador 2 gana!");
            }
            if ((this.tablero[0][2] == this.jugador1) && (this.tablero[1][2] == this.jugador1) && (this.tablero[2][2]==this.jugador1)){
		this.winner = true;
                System.out.println("¡Jugador 1 gana!");
            }
        this.Gamer.setGanador(this.winner);
        this.Gamer = new Jugadores(this.Gamer.getRole(), this.Gamer.getTurno() -1,this.winner);
        this.contador = this.Gamer.getTurno();
        this.winner = this.Gamer.isGanador();
        this.pila = new PilaJuego(this.tablero,this.Gamer);
        gf.empilar(this.pila);
        return this.winner;
       }                              

    
//Dibujar el tablero
    public void drawtablero(int w, int q, char jugador){
        for ( int x = 0; x < this.tablero.length; x++ ){
            System.out.print(" | ");
            for ( int y = 0; y < this.tablero[0].length; y++ ){
                this.tablero[w][q]=jugador;
                System.out.print( this.tablero[x][y] + " | ");
            }
            System.out.println( "\n---------------");
        }  
    }

}
