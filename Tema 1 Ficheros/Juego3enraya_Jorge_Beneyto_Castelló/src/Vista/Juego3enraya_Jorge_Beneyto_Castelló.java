/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.GestionFicheros;
import Controlador.GestionarJuego;
import java.io.File;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Juego3enraya_Jorge_Beneyto_Castelló {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        GestionarJuego gj = new GestionarJuego(); 
        GestionFicheros gf = new GestionFicheros(new File("3enraya.obj"));
        int option = 0;
        
        System.out.println("!3!3!3!");
        System.out.println("3 en ---");
        System.out.println("!3!3!3!");
        
        do {
            menu();
            System.out.println("Elige una opción: ");
            option = tec.nextInt();
            
            switch (option){
                case 1:
                    System.out.println("Bienvenidos al 3 en raya, que gane el mas astuto");
                     gj.inicialitzartablero();
                     gj.changetablero();
                    break;
                case 2:
                    System.out.println("Gracias por jugar a nuestro juego, esperamos que te hayas divertido y vuelvas pronto!!!");
                    gf.eliminarFichero();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opción no valida");
                    break;
            }         
        } while (option != 0);

    }
    
    public static void menu(){
        System.out.println("1. Jugar");
        System.out.println("3. Salir");
    }
    
    public static void submenu(){
        System.out.println("1. Continuar");
        System.out.println("2. Atras");
    }
    

    public static void menuconfirmar(){
        System.out.println("1. Volver otra vez atras");
        System.out.println("2. Continuar Jugado");
    }

}
