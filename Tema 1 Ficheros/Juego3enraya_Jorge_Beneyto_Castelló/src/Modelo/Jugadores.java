/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class Jugadores implements Serializable {
    private int role;
    private int turno;
    private boolean ganador;

    public Jugadores(int role, int turno, boolean ganador) {
        this.role = role;
        this.turno = turno;
        this.ganador = ganador;
    }

    public int getRole() {
        return role;
    }

    public int getTurno() {
        return turno;
    }

   

    public boolean isGanador() {
        return ganador;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    

    public void setGanador(boolean ganador) {
        this.ganador = ganador;
    }

    @Override
    public String toString() {
        return "Jugadores{" + "role=" + role + ", turno=" + turno  + ", ganador=" + ganador + '}';
    }
    
    
}
