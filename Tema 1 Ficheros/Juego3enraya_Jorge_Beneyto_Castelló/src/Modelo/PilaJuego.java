/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class PilaJuego implements Serializable {
    char tablero [][] = new char [3][3];
    Jugadores jugador; 

    public PilaJuego(char [][] tablero, Jugadores jugador) {
        this.tablero = tablero;
        this.jugador = jugador;
    }

    public char[][] getTablero() {
        return tablero;
    }

    public Jugadores getJugador() {
        return jugador;
    }

    public void setTablero(char[][] tablero) {
        this.tablero = tablero;
    }

    public void setJugador(Jugadores jugador) {
        this.jugador = jugador;
    }

    @Override
    public String toString() {
        return "PilaJuego{" + "tablero=" + tablero + ", jugador=" + jugador + '}';
    }
    
    
    
}
