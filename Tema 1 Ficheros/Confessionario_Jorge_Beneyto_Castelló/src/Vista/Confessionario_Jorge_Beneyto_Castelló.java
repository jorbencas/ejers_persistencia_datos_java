/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.Confesionario;
import Controlador.GestionConfesionario;
import Modelo.Vendedores;
import Modelo.Vehiculo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Confessionario_Jorge_Beneyto_Castelló {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // TODO code application logic here
        Scanner teclado = new Scanner (System.in); //Creación de un objeto Scanner
        String marca, modelo, matricula, color, categoria, nombre;
        int precio, potencia, codigo, option = 0;
                 

        GestionConfesionario gf = null;
        Confesionario concesionario = new Confesionario();
        Vendedores vend;
        Vehiculo veh;
         
        do {    
            menu();
            option = teclado.nextInt();
            switch(option){
            case 1:
               //ALTA VEHICULOS
                System.out.println("Introduce la matrícula del vehículo");
                teclado.nextLine();
                matricula = teclado.nextLine();

                System.out.println("Introduce el modelo del vehículo");
                modelo = teclado.nextLine();

                System.out.println("Introduce la marca del vehículo");
                marca = teclado.nextLine();

                System.out.println("Introduce la potencia del vehículo");
                potencia = teclado.nextInt();

                System.out.println("Introduce el color del vehículo");
                teclado.nextLine();
                color = teclado.nextLine();

                System.out.println("Introduce el precio del vehículo");
                precio = teclado.nextInt();
      
                veh = new Vehiculo(marca,modelo,matricula,potencia, color, precio);
               
                concesionario.getVehiculos().add(veh);
                gf.insertarVehiculo();
                
               break;
            case 2:
                //ALTA VENDEDORES  
                System.out.println("Introduce el codigo del vendedor");
                codigo = teclado.nextInt();

                System.out.println("Introduce el nombre del vendedor");
                teclado.nextLine();
                nombre = teclado.nextLine();

                System.out.println("Introduce la categoría del vendedor");
                categoria = teclado.nextLine();

                vend = new Vendedores(codigo, nombre, categoria);
               
                concesionario.getVendedores().add(vend);
                gf.insertarVendedor();
                
                break;
            case 3:
                 //VENTA DE UN VEHICULO POR PARTE DE UN VENDEDOR
                System.out.println("Introduce código del vendedor\n");
                codigo = teclado.nextInt();

                System.out.println("Introduce matrícula del vehículo\n");
                teclado.next();
                matricula = teclado.nextLine();

                vend = gf.buscarVendedor(codigo);
                veh = gf.buscarVehiculo(matricula);

                vend.getLista_vendidos().add(veh);
                System.out.println("Compra realizada.\n");
              
                break;
            case 4:
                //CARGA DE LOS VEHICULOS DESDE EL FICHERO VEHICULOS.DAT
                System.out.println("Cargando...");
                gf.cargarVehiculos();
                break;
            case 5:
                //CARGA DE LOS VENDEDORES DESDE EL FICHERO VENDEDORES.DAT
                System.out.println("Cargando...");
                gf.cargarVendedores();
                break;
            case 6:
                System.out.println("------------------"
                          + "---------------Adiós y gracias por usar nuestra aplicación---------"
                          + "------------------"); 
                System.exit(0);
                break;
            default:
                System.out.println("OPCIÓN NO VÁLIDA"); 
            }
        } while (option != 0 );
    }
   
    public static void menu(){
          System.out.println("1. Introducir Vehiculos");
          System.out.println("2. Introducir Vendedores");
          System.out.println("3. Venta de un vehiculo por parte de un vendedor");
          System.out.println("4. Listar vehiculos");
          System.out.println("5. Listar Vendedores");
          System.out.println("6. Salir");
    }
}
