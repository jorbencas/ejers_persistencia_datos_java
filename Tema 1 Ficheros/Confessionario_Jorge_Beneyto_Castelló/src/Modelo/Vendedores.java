/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.Vehiculo;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class Vendedores implements Serializable {
   
    private int codigo;
    private String nombre;
    private String categoria;
    private ArrayList<Vehiculo> lista_vendidos;

    public Vendedores(int codigo, String nombre, String categoria) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.categoria = categoria;
        this.lista_vendidos = new ArrayList<Vehiculo>();
    }

      public void añadirVenta(Vehiculo v){
        lista_vendidos.add(v);
    }

    public void mostrarVentas(){
        for (Vehiculo venta : lista_vendidos) {
            System.out.println(venta);
        }
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public ArrayList<Vehiculo> getLista_vendidos() {
        return lista_vendidos;
    }

    public void setLista_vendidos(ArrayList<Vehiculo> lista_vendidos) {
        this.lista_vendidos = lista_vendidos;
    }

    public Vendedores() {
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", categoria='" + categoria + '\'' +
                ", lista_vendidos=" + lista_vendidos +
                '}';
    }
    
    
}
