/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Vehiculo;
import Modelo.Vendedores;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class Confesionario {
    private ArrayList<Vendedores> vendedores ;
    private ArrayList<Vehiculo> vehiculos ;

      public Confesionario() {
        this.vehiculos = new ArrayList<Vehiculo>();
        this.vendedores = new ArrayList<Vendedores>();
    }

    public ArrayList<Vendedores> getVendedores() {
        return vendedores;
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }
    
}
