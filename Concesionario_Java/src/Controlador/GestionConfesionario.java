/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Vendedores;
import Modelo.Vehiculo;
import java.io.*;

/**
 *
 * @author jorge
 */
public class GestionConfesionario {
     File file = new File("vendedores.dat");
     File fileveh = new File("vehiculos.dat");
     Confesionario concesionario = new Confesionario();
       
    public void insertarVendedor(){
        
          MyObjectOutputStream Salida;
          ObjectOutputStream Salida2;
          
            if(this.file.exists()){
                try {
                    Salida = new MyObjectOutputStream(new FileOutputStream(this.file, true));
                    for (Vendedores v : this.concesionario.getVendedores()) {
                        
                        Salida.writeObject(v);
                    }
                    this.concesionario.getVendedores().clear();
                    Salida.close();
                } catch(IOException e){
                     System.out.println("Fin del fichero");
                }    
            }else{
                try {
                    Salida2 = new ObjectOutputStream(new FileOutputStream(file, true));
                    for (Vendedores v : this.concesionario.getVendedores()) {
                        Salida2.writeObject(v);
                    }
                    this.concesionario.getVendedores().clear();
                    Salida2.close();
                } catch(IOException e){
                    System.out.println("Fin del fichero");
                }
            }  
    }
       
    public void insertarVehiculo() {
          MyObjectOutputStream Salida;
          ObjectOutputStream Salida2;
          
            if(this.fileveh.exists()){
                try {
                    Salida = new MyObjectOutputStream(new FileOutputStream(this.fileveh, true));
                     for (Vehiculo v : this.concesionario.getVehiculos()) {
                        Salida.writeObject(v);
                     }
                    this.concesionario.getVehiculos().clear();
                    Salida.close();
                } catch(IOException e){
                     System.out.println("Fin del fichero");
                }    
            }else{
                try {
                    Salida2 = new ObjectOutputStream(new FileOutputStream(file, true));
                     for (Vehiculo v : this.concesionario.getVehiculos()) {
                        Salida2.writeObject(v);
                     }
                    this.concesionario.getVehiculos().clear();
                    Salida2.close();
                } catch(IOException e){
                    System.out.println("Fin del fichero");
                }
            }  
    }

    public void cargarVendedores() throws IOException, ClassNotFoundException{
         System.out.println("LEYENDO FICHERO VENDEDORES.DAT");

        try {
            ObjectInputStream entrada2 = new ObjectInputStream(new FileInputStream("vendedores.dat"));

            Vendedores ven2;
            //MIENTRAS HAYA OBJETOS, SIGUE LEYENDO
            while ((ven2 = (Vendedores) entrada2.readObject()) != null) {
                System.out.println(ven2);
                ven2.mostrarVentas();
            }
            entrada2.close();
            //AL FINALIZAR EL FICHERO, EL READOBJECT LANZA UNA EXCEPCIÓN, QUE CAPTURAMOS AQUÍ
        } catch (EOFException e) {
            System.out.println("Fin del fichero");
        }

    }

    public void cargarVehiculos() throws IOException, ClassNotFoundException    {
        System.out.println("LEYENDO FICHERO VEHICULOS.DAT");
        try {
            ObjectInputStream entrada = new ObjectInputStream(new FileInputStream("vehiculos.dat"));
            Vehiculo veh2;
            //MIENTRAS HAYA OBJETOS, SIGUE LEYENDO
            while ((veh2 = (Vehiculo) entrada.readObject()) != null) {
                System.out.println(veh2);
            }
            entrada.close();
            //AL FINALIZAR EL FICHERO, EL READOBJECT LANZA UNA EXCEPCIÓN, QUE CAPTURAMOS AQUÍ
        } catch (EOFException e) {
            System.out.println("Fin del fichero");
        }
    }
    
    public Vendedores buscarVendedor( int codigo) {
        for (Vendedores v : this.concesionario.getVendedores()) {
            if (v.getCodigo() == codigo) {
                return v;
            }
        }
        return null;
    }

    public Vehiculo buscarVehiculo( String matricula) {
        for (Vehiculo v : this.concesionario.getVehiculos()) {
            if (v.getMatricula().equalsIgnoreCase(matricula)) {
                return v;
            }
        }
        return null;
    }
}