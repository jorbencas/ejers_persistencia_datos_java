package Modelo;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Vehiculo implements Serializable {
    private String marca;
    private String modelo;
    private String matricula;
    private int potencia;
    private String color;
    private int precio;

    public Vehiculo(String marca, String modelo, String matricula, int potencia, String color, int precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
        this.potencia = potencia;
        this.color = color;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", matricula='" + matricula + '\'' +
                ", potencia='" + potencia + '\'' +
                ", color='" + color + '\'' +
                ", precio=" + precio +
                '}';
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    public void writteObject(  ObjectOutputStream Salida) throws IOException{
        String precioencriptado="";
        
        Salida.writeUTF(marca);
        Salida.writeUTF(modelo);
        Salida.writeUTF(matricula);
        Salida.writeInt(potencia);
        Salida.writeUTF(color);
        precioencriptado = encriptar(this.getPrecio());
        Salida.writeUTF(precioencriptado);

    }
    
    public String encriptar(int precio){
        String precioencriptado ="";
        String texto = Integer.toString(precio);
        char caracter;
         for(int i = 0; i < texto.length(); i++){
             caracter = (char) (texto.charAt(i) + 48);
             precioencriptado += caracter;
         }
         return precioencriptado;
    }   

    public void readObject(  ObjectInputStream entrada)throws IOException{
        String tmpprecio ="";
        marca = entrada.readUTF();
        modelo = entrada.readUTF();
        matricula =  entrada.readUTF();
        potencia =  entrada.readInt();
        color =  entrada.readUTF();
        tmpprecio = entrada.readUTF();
        precio = desencriptar(tmpprecio);
    }
    
    public int desencriptar(String predioencriptado ){
       int precio = 0;
        String texto = Integer.toString(precio);
        char caracter;
         for(int i = 0; i < predioencriptado.length(); i++){
             caracter = (char) (predioencriptado.charAt(i) - 48);
             precio += Integer.valueOf(caracter);
         }
        return precio;
    }
}
